#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May 22 18:51:15 2020

@author: dandy
"""

import h5py
import numpy as np
import matplotlib.pyplot as plt
file = h5py.File('DTM.hdf5', 'r')
DTM = np.array(file['HEIGHT'])
PHI = np.array(file['LATITUDE'])
THT = np.array(file['LONGITUDE'])
file.close()

plt.figure(figsize=(20,10))
mmm,ttt = np.meshgrid(THT, PHI)
plt.contourf(mmm,ttt,DTM, 20)
plt.colorbar()


