#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# %%
"""
Created on Fri May 22 15:40:36 2020

@author: dandy
"""
# %%
from scipy.special import lpmn
import numpy as np
import matplotlib.pyplot as plt
import math
import h5py
import time
# %%
def rad(theta, phi, cfs):
    #phi is between 0 and 2*np.pi
    theta = deg2rad(theta)
    phi = deg2rad(phi)
    N = len(cfs)-1
    pmn = lpmn(N,N,np.sin(phi))[0]
    result = 0
    for n in range(N+1):
        for m in range(n+1):
            result += ((cfs[n][m][0])*np.cos(m*theta) + (cfs[n][m][1])*np.sin(m*theta))*pmn[m][n] #* norm_factor(n,m)

    return result

# def norm_factor(n,m):
#     return ( (2*n+1) / (4*np.pi) )**0.5 * (math.factorial(n-m)/math.factorial(n+m))**0.5
def norm_factor(n,m):
    return ( (2*n+1) )**0.5 * (math.factorial(n-m)/math.factorial(n+m))**0.5
def deg2rad(deg):
    return deg/180.*np.pi

# %%
#Read spherical harmonics coefficients:
direct =  './data/'
file = open('MoonTopo2600p.shape.sh', 'r')
data = file.readlines()
data = [np.asarray(list(filter(None, dat.strip().split(' '))), dtype = np.float64)  for dat in data]

# %%
N = 151
sh_coefs = np.zeros([N,N,2])
for i in range(int(N*(N+1)/2.)):
    row = data[i]
    sh_coefs[int(row[0])][int(row[1])] = row[2:4]

# %%
start = time.time()
tres = 180
pres = 181

tht = np.linspace(-180, 180, tres)
ph  = np.linspace(90, -90,  pres)

theta = deg2rad(tht)
phi = deg2rad(ph )

ttt, ppp = np.meshgrid(theta, phi)

NN = len(sh_coefs)-1

pmn = np.array([[lpmn(NN,NN,np.sin(angle))[0] for angle in ppp[i]] for i in range(len(ppp))])

DTM = np.zeros_like(ttt) 

for n in range(NN+1):
    for m in range(n+1):
        DTM += ((sh_coefs[n][m][0])*np.cos(m*ttt) + (sh_coefs[n][m][1])*np.sin(m*ttt))*pmn[:, :, m, n] * norm_factor(n,m)


print(time.time() - start)

# %%
DTM -= sh_coefs[0,0,0]
# %%
tht_shifted = tht - 180
plt.figure(figsize=(20,10))
#ttt,vvv= np.meshgrid(tht, phi)#tht_shifted, phi)
plt.contourf(ttt, ppp,DTM, 50)#,cmap='CMRmap')
plt.grid(linestyle='--')
plt.colorbar()
# %%
f = h5py.File('DTM.hdf5', 'w')
f.create_dataset("HEIGHT", data=DTM)
f.create_dataset("LONGITUDE", data=tht)
f.create_dataset("LATITUDE", data=phi)
f.attrs['REFERENCE SPHERE RADIUS'] = sh_coefs[0,0,0]
f.close()

# %%
tht

# %%
