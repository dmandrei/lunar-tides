# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.10.2
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# +
import salvus.namespace as sn
from scipy.special import sph_harm, lpmn
from salvus.mesh import simple_mesh
from salvus.flow import api
from salvus.mesh.optimize_dt import optimize_dt, optimize_dt_locally
from salvus.flow import simple_config, api
import numpy as np
import os
import matplotlib.pyplot as plt
import sys
sys.path.append('./mass/')
from elemental_matrices import get_mass_matrix
from scipy import interpolate
import h5py
import random
from functions import *

from phobos_properties import R_PHOBOS, M_PHOBOS, V_PHOBOS

def meshing(outer_buf = 10, elements_per_wavelength = 1., 
            min_period_in_seconds = 0.5, rl = [1/5., 2/5., 3/5., 4/5.,], DTM = True, min_radius = 0.):
    assert type(outer_buf) == int
    
    mc = simple_mesh.Globe3D()
    mc.basic.elements_per_wavelength = elements_per_wavelength
    mc.basic.model = "moon.bm"
    mc.basic.min_period_in_seconds = min_period_in_seconds
    mc.advanced.tensor_order = 4
    if min_radius > 0:
        mc.spherical.min_radius = min_radius
    if outer_buf > 0:
        mc.gravity_mesh.add_exterior_domain = True
        mc.gravity_mesh.nelem_buffer_outer = outer_buf
    m = mc.create_mesh()
    
    RHOidx = RHO_indeces(m, rl)
    
    if DTM:
        lat,long,dem = read_phobos('./DTM/DTM.hdf5')

        eps = 0.00001
        lat  = np.linspace(eps,     np.pi - eps, dem.shape[1])
        long = np.linspace(eps, 2 * np.pi - eps, dem.shape[0])

        m.add_dem_3D(lat, long, dem.T ,mode='spherical_full', z0=0.0, zref=R_PHOBOS)
        m.apply_dem()
    
    return m, RHOidx

def RHO_indeces(m, rl, R_planet):
    
    radii = np.linalg.norm( m.get_element_nodes(), axis = 2)
    ll    = [rl * R_planet for rl in rl]
    ll.append(0.), ll.append(R_planet+1e-1)
    ll = sorted(ll)
    RHOidx = list()
    
    if 'external' in m.elemental_fields.keys():
        for i in range(1, len(ll)):
            dx = abs(ll[i-1] - ll[i])
            RHOidx.append( np.where( abs(radii[(m.elemental_fields['external'] < 0.9)] \
                                         - ll[i-1] - dx/2.) <= dx/2.) ) 
    else:
        for i in range(1, len(ll)):
            dx = abs(ll[i-1] - ll[i])
            RHOidx.append( np.where( abs(radii - ll[i-1] - dx/2.) <= dx/2.) ) 
            
    return np.array(RHOidx)

def setRHO(mesh, dens, RHOidx):
    assert len(dens) == len(RHOidx)
    if 'RHO' not in mesh.element_nodal_fields.keys():
        mesh.attach_field('RHO', np.zeros(tuple([mesh.nelem, mesh.nodes_per_element])))
    for i in range(len(dens)):
        mesh.element_nodal_fields['RHO'][tuple(RHOidx[i])] = dens[i] 

def setprop(mesh, propval, RHOidx, prop):
    assert len(propval) == len(RHOidx)
    assert type(prop) == str
    if prop not in mesh.element_nodal_fields.keys():
        mesh.attach_field(prop, np.zeros(tuple([mesh.nelem, mesh.nodes_per_element])))
    for i in range(len(propval)):
        mesh.element_nodal_fields[prop][tuple(RHOidx[i])] = propval[i]    
        
#Doesn't properly find the elements
def add_regolith_layer(mesh, thickness = 100., rho = 1500., mu = 0.1*10**9):
    internal_idx = np.where(mesh.elemental_fields['external'] < 0.5)
    r, phi, theta = cart2sph(mesh.get_element_nodes()[internal_idx])
    phi += np.pi
    minr = np.min(np.linalg.norm(mesh.get_element_nodes()[mesh.side_sets['r1']], axis=1))
    cond = r > minr - thickness
    
    cfs = open_DTM_coefs()
    DTMofinterest = np.zeros(phi.shape)
    DTM = DTMfunc(phi[cond], theta[cond], cfs)
    DTMofinterest[cond] = DTM
    
    cond = (DTMofinterest > 0.) * (abs(r - DTMofinterest) <= thickness)
    
    rtmp = mesh.element_nodal_fields['RHO']
    rrtmp = rtmp[internal_idx]
    rrtmp[cond] = rho
    rtmp[internal_idx] = rrtmp
    
    print('Regolith layer was added')
    
def porous_elements(mesh, porosity_on_surface = True):
    fullset = set(range(0, mesh.nelem))
    if porosity_on_surface:
        sideset = set()
    else:
        #do not touch side_set elements
        sideset = set(mesh.side_sets['r1'][0])

    if 'external' in mesh.elemental_fields.keys():
        externalset = set(np.where(mesh.elemental_fields['external'] > 0.5)[0])
        innerel = tuple(fullset - sideset - externalset)
    else:    
        innerel = tuple(fullset - sideset)
    return innerel

def porous_phobos_mesh_spheres(
                       porosity = 0.3, fluid_rho = 1., 
                       grain_mu = 0.1, fluid_mu = 10**-8,
                       epw = 1., mps = 1., buffer = 20, 
                       porosity_on_surface = True,
                       elements_or_nodes = 'nodes'):
    
    #what must be the homogeneous grain density to gain Phobos' bulk density:
    RHO_PHOBOS = M_PHOBOS/V_PHOBOS
    grain_rho = (porosity * fluid_rho - RHO_PHOBOS) / (porosity - 1)

    #initially setting homogeneous phobos model:
    rl = []
    rho = [grain_rho]*(len(rl) + 1)

    pois  = 0.28
    mu    = [grain_mu] *(len(rl) + 1)
    mu    = np.array([m *10**9 for m in mu])
    kappa = np.array([kappaM(mu, pois) for mu in mu])

    fluid_kappa = kappaM(fluid_mu, pois)
    
    mesh, RHOidx = meshing(elements_per_wavelength = epw, 
                         min_period_in_seconds   = mps, 
                         outer_buf = buffer,  rl = rl)

    setprop(mesh, rho,   RHOidx, 'RHO')
    setprop(mesh, mu,    RHOidx, 'MU')
    setprop(mesh, kappa, RHOidx, 'KAPPA')
    
    mesh.find_side_sets()
    mass_matrix = get_mass_matrix(mesh)
    
    
    if 'external' in mesh.elemental_fields.keys():
        internal   = np.where(mesh.elemental_fields['external'] < 0.5)
        volume     = np.sum(mass_matrix[internal])
    else:
        volume     = np.sum(mass_matrix)
        
    print(f'Model Phobos volume: \t {volume/10**9}')
    
    model_mass = mass(mesh, mass_matrix)
    
    innerset = porous_elements(mesh, porosity_on_surface)
    
    cur_mass = model_mass
    
    if elements_or_nodes == 'nodes':
        
        while cur_mass > M_PHOBOS:  
            #here the code takes 10 nodes simultaneously (k=10) and deal with them. 
            #You can change it with k but time costs are formidable
            ind = (random.sample(innerset, k=10), np.random.randint(0, mesh.nodes_per_element))
            mesh.element_nodal_fields['RHO'][ind]    = fluid_rho
            mesh.element_nodal_fields['MU'][ind]     = fluid_mu * 10**9
            mesh.element_nodal_fields['KAPPA'][ind]  = fluid_kappa

            cur_mass = mass(mesh, mass_matrix)
            
    elif elements_or_nodes == 'elements':
            #Faster way
        while cur_mass > M_PHOBOS:  

            ind = (random.choice(innerset))
            mesh.element_nodal_fields['RHO'][ind]    = fluid_rho
            mesh.element_nodal_fields['MU'][ind]     = fluid_mu
            mesh.element_nodal_fields['KAPPA'][ind]  = fluid_kappa

            cur_mass = mass(mesh, mass_matrix)
    
    print(f'Model Phobos mass: \t {cur_mass/10**16}e16 kg')
    return mesh

def porous_phobos_mesh(porosity = 0.3, fluid_rho = 1., 
                       grain_mu = 0.1, fluid_mu = 10**-8,
                       epw = 1., mps = 1., buffer = 20, 
                       porosity_on_surface = True,
                       elements_or_nodes = 'nodes'):
    
    #what must be the homogeneous grain density to gain Phobos' bulk density:
    RHO_PHOBOS = M_PHOBOS/V_PHOBOS
    grain_rho = (porosity * fluid_rho - RHO_PHOBOS) / (porosity - 1)

    #initially setting homogeneous phobos model:
    rl = []
    rho = [grain_rho]*(len(rl) + 1)

    pois  = 0.28
    mu    = [grain_mu] *(len(rl) + 1)
    mu    = np.array([m *10**9 for m in mu])
    kappa = np.array([kappaM(mu, pois) for mu in mu])

    fluid_kappa = kappaM(fluid_mu, pois)
    
    mesh, RHOidx = meshing(elements_per_wavelength = epw, 
                         min_period_in_seconds   = mps, 
                         outer_buf = buffer,  rl = rl)

    setprop(mesh, rho,   RHOidx, 'RHO')
    setprop(mesh, mu,    RHOidx, 'MU')
    setprop(mesh, kappa, RHOidx, 'KAPPA')
    
    mesh.find_side_sets()
    mass_matrix = get_mass_matrix(mesh)
    
    
    if 'external' in mesh.elemental_fields.keys():
        internal   = np.where(mesh.elemental_fields['external'] < 0.5)
        volume     = np.sum(mass_matrix[internal])
    else:
        volume     = np.sum(mass_matrix)
        
    print(f'Model Phobos volume: \t {volume/10**9}')
    
    model_mass = mass(mesh, mass_matrix)
    
    innerset = porous_elements(mesh, porosity_on_surface)
    
    cur_mass = model_mass
    
    if elements_or_nodes == 'nodes':
        
        while cur_mass > M_PHOBOS:  
            #here the code takes 10 nodes simultaneously (k=10) and deal with them. 
            #You can change it with k but time costs are formidable
            ind = (random.sample(innerset, k=10), np.random.randint(0, mesh.nodes_per_element))
            mesh.element_nodal_fields['RHO'][ind]    = fluid_rho
            mesh.element_nodal_fields['MU'][ind]     = fluid_mu * 10**9
            mesh.element_nodal_fields['KAPPA'][ind]  = fluid_kappa

            cur_mass = mass(mesh, mass_matrix)
            
    elif elements_or_nodes == 'elements':
            #Faster way
        while cur_mass > M_PHOBOS:  

            ind = (random.choice(innerset))
            mesh.element_nodal_fields['RHO'][ind]    = fluid_rho
            mesh.element_nodal_fields['MU'][ind]     = fluid_mu
            mesh.element_nodal_fields['KAPPA'][ind]  = fluid_kappa

            cur_mass = mass(mesh, mass_matrix)
            print(f'Model Phobos mass: \t {cur_mass/10**16}e16 kg')
    return mesh