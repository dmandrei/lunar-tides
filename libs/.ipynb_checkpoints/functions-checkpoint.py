# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.6.0
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# +
import salvus.namespace as sn
from scipy.special import sph_harm, lpmn
from salvus.mesh import simple_mesh
from salvus.flow import api
from salvus.mesh.optimize_dt import optimize_dt, optimize_dt_locally
from salvus.flow import simple_config, api
import numpy as np
import os
import matplotlib.pyplot as plt
import sys
sys.path.append('./mass/')
from elemental_matrices import get_mass_matrix
from scipy import interpolate
import h5py
import random


def read_phobos(fname):
    f = h5py.File(fname, 'r')
    DTM = np.array(f['HEIGHT'])
    long = np.array(f['LONGITUDE'])
    lat = np.array(f['LATITUDE'])
    return lat, long, DTM

def mass(m, vol):
    return np.sum(vol * m.element_nodal_fields['RHO'])

def moi(mesh):
    mass_matrix = get_mass_matrix(mesh)
    mass  = (mesh.element_nodal_fields['RHO'] * mass_matrix)

    crd = mesh.get_element_nodes()
    x,y,z = crd[:,:,0], crd[:,:,1], crd[:,:,2]
    x2, y2, z2 = x**2, y**2, z**2

    Ixx = np.sum(mass * (y2 + z2))
    Iyy = np.sum(mass * (x2 + z2))
    Izz = np.sum(mass * (x2 + y2))

    Ixy = np.sum((-1) * mass *x *y)
    Iyx = Ixy

    Ixz = np.sum((-1) * mass *x *z)
    Izx = Ixz

    Iyz = np.sum((-1) * mass *y *z)
    Izy = Iyz

    moi_matrix = np.array([
    [Ixx, Ixy, Ixz],
    [Iyx, Iyy, Iyz],
    [Izx, Izy, Izz],                      
    ])

    moi_pr = sorted(np.linalg.eigvals(moi_matrix))

    return moi_pr, moi_matrix

def elmods_from_seismv(vp, vs, rho):
    #elastic moduli from seismic velocities and density
    mu = [vs[i]**2 * rho[i] for i in range(len(vs))]
    kappa = [vp[i]**2 * rho[i] - 4/3.*mu[i] for i in range(len(vs))]
    return {'mu':mu, 'kappa':kappa}
       
def kappaM(sh, pois):
    return 2/3. * sh * (1 + pois) / (1 - 2*pois)

def cart2sph(coord):
    #Cartesian coordinates to spherical
    dims  = coord.shape[0:2]

    r     = np.linalg.norm(coord, axis=2)

    rxy   = np.linalg.norm(coord[:,:,0:2], axis=2)
    phi = np.zeros(dims)
    
    cond = (rxy > 0.) * (coord[:,:,0] >= 0.) * (coord[:,:,1] >= 0.)
    phi[cond]   = np.arcsin( coord[:,:,1][cond] / rxy[cond] ) 
    
    cond = (rxy > 0.) * (coord[:,:,0] <  0.) * (coord[:,:,1] >= 0.)
    phi[cond]   = np.arccos( coord[:,:,0][cond] / rxy[cond] )
    
    cond = (rxy > 0.) * (coord[:,:,0] <  0.) * (coord[:,:,1] <  0.)
    phi[cond]   = (-1) * np.arcsin( coord[:,:,1][cond] / rxy[cond] ) + np.pi
    
    cond = (rxy > 0.) * (coord[:,:,0] >= 0.) * (coord[:,:,1] <  0.)
    phi[cond]   = 2*np.pi - np.arccos( coord[:,:,0][cond] / rxy[cond] ) 
    
    cond = rxy < 1e-8
    phi[cond]   = 0.
    
    cond = r > 0
    theta = np.zeros(dims)
    theta[cond] = np.arccos( coord[:,:,2][cond] / r[cond] )
    theta[r < 1e-8] = 0.

    return np.array([r, phi, theta])

def open_DTM_coefs(file = './DTM/data/TABLEA2.DAT'):
    file = open(file, 'r')
    data = file.readlines()
    data = [np.asarray(dat.strip().split(' '*3), dtype = np.float64) for dat in data]
    N = 45+1
    sh_coefs = np.zeros([N,N,4])
    for row in data:
        sh_coefs[int(row[0])][int(row[1])] = row[2:6]
    return sh_coefs

def DTMfunc(phi, theta, cfs):
    N = len(cfs)-1
    pmn = np.array(
    [lpmn(N,N,np.sin(phi[i]))[0] for i in range(phi.shape[0])])
    result = np.zeros(phi.shape)
    for n in range(N+1):
        for m in range(n+1):
            result += ((cfs[n,m][0])*np.cos(m*theta) + (cfs[n,m][2])*np.sin(m*theta))\
            *pmn[:,m,n]
    return result

def porosity(bulkrho, grainrho, fluidrho):
    assert fluidrho <= bulkrho 
    assert bulkrho  <= grainrho 
    porosity = (grainrho - bulkrho)/(grainrho - fluidrho)
    return porosity


def get_phobos_long_libration(mean_anomaly):
    #Gives forced longitudal libration magnitude [Burmeister et al., 2018]
    return  np.radians( -1.143 * np.sin( mean_anomaly + np.pi ) )

def true_anomaly2mean_anomaly(true_anomaly, ecc):
    #series decomposition from wikipedia (https://en.wikipedia.org/wiki/Mean_anomaly)
    return true_anomaly \
+ 2 * ecc * np.sin(true_anomaly) \
+ (3/4. * ecc**2 + 1/8. * ecc**4) * np.sin(2 * true_anomaly) \
- 1/3.  * ecc**3 * np.sin(3 * true_anomaly)\
+ 5/32. * ecc**4 * np.sin(4 * true_anomaly)
