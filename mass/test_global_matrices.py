#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sympy

'''
Tests

:copyright:
    Martin van Driel (Martin@vanDriel.de), 2019
:license:
    None
'''
import numpy as np
from global_matrices import get_gather_scatter, get_mass_matrix_points

import pytest
from salvus.mesh.structured_grid_2D import StructuredGrid2D
from salvus.mesh.structured_grid_3D import StructuredGrid3D


def test_get_gather_scatter():
    # 2D
    m = StructuredGrid2D.rectangle(2, 2).get_unstructured_mesh()
    p = m.points[m.connectivity].reshape((m.nelem * m.nodes_per_element,
                                          m.ndim))

    gather, scatter, global_ids = get_gather_scatter(p)
    valence = scatter.dot(gather.dot(np.ones(p.shape[0])))

    valence_ref = np.array([1., 2., 2., 4., 2., 1., 4., 2., 2., 4., 1., 2., 4.,
                            2., 2., 1.])

    np.testing.assert_allclose(valence, valence_ref, atol=1e-15)

    # 3D
    m = StructuredGrid3D.cube(2, 2, 2).get_unstructured_mesh()
    p = m.points[m.connectivity].reshape((m.nelem * m.nodes_per_element,
                                          m.ndim))

    gather, scatter, global_ids = get_gather_scatter(p)
    valence = scatter.dot(gather.dot(np.ones(p.shape[0])))

    valence_ref = np.array([1., 2., 2., 4., 2., 4., 4., 8., 2., 4., 4., 8., 1.,
                            2., 2., 4., 2., 1., 4., 2., 4., 2., 8., 4., 4., 2.,
                            8., 4., 2., 1., 4., 2., 2., 4., 1., 2., 4., 8., 2.,
                            4., 4., 8., 2., 4., 2., 4., 1., 2., 4., 2., 2., 1.,
                            8., 4., 4., 2., 8., 4., 4., 2., 4., 2., 2., 1.])

    np.testing.assert_allclose(valence, valence_ref, atol=1e-15)


@pytest.mark.parametrize("order", [3, 4, 5, 6, 7, 8, 9, 10, 15, 20])
def test_get_mass_matrix_points(order):
    # 2D integration test
    sg = StructuredGrid2D.rectangle(3, 5, min_x=-1., min_y=-1.)
    m = sg.get_unstructured_mesh()
    m.change_tensor_order(order)

    M, p = get_mass_matrix_points(m)

    # analytical function and integral
    x, y = p.T
    f = x ** order + y ** (2 * order - 2)
    n1 = order + 1
    n2 = 2 * order - 1
    If = (1 ** n1 / n1 - (-1) ** n1 / n1) * 2
    If += (1 ** n2 / n2 - (-1) ** n2 / n2) * 2

    np.testing.assert_allclose(M.dot(f).sum(), If, atol=1e-15)

    # 3D integration test
    sg = StructuredGrid3D.cube(3, 5, 2, min_x=-1., min_y=-1., min_z=-1.)
    m = sg.get_unstructured_mesh()
    m.change_tensor_order(order)

    M, p = get_mass_matrix_points(m)

    # analytical function and integral
    x, y, z = p.T
    f = x ** order + y ** (2 * order - 2) + z
    n1 = order + 1
    n2 = 2 * order - 1
    If = (1 ** n1 / n1 - (-1) ** n1 / n1) * 4
    If += (1 ** n2 / n2 - (-1) ** n2 / n2) * 4
    If += (1 ** 2 / 2 - (-1) ** 2 / 2) * 4

    np.testing.assert_allclose(M.dot(f).sum(), If, atol=1e-15)
